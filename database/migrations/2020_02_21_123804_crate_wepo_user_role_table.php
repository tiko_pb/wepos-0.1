<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrateWepoUserRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wepos_user_role', function (Blueprint $table) {
            $table->mediumInteger('wepos_user_role_id')->autoIncrement();
            $table->decimal('wepos_user_id', 10, 0);
            $table->decimal('wepos_role_id', 10, 0);
            $table->decimal('created_by', 10, 0);
            $table->decimal('updated_by', 10, 0);
            $table->boolean('is_active', true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wepos_user_role');
    }
}
