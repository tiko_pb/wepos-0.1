<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeposAptResepuser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wepos_apt_resepuser', function (Blueprint $table) {
            $table->bigIncrements('wepos_apt_resepuser_id');
            $table->decimal('created_by', 10, 0);
            $table->decimal('updated_by', 10, 0);
            $table->string('role_value', 50)->nullable();
            $table->string('role_name', 50)->nullable();
            $table->boolean('is_active', true);  //PERBEDAAN USER DAN DOCTOR
            $table->string('user_name_ru',50);
            $table->string('user_address_ru',50);
            $table->string('user_contact_number_ru',50);
            $table->boolean('is_doctor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wepos_apt_resepuser');
    }
}
