<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeposRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wepos_role', function (Blueprint $table) {
            $table->mediumInteger('wepos_role_id')->autoIncrement();
            $table->decimal('created_by', 10, 0);
            $table->decimal('updated_by', 10, 0);
            $table->string('role_value', 50)->nullable();
            $table->string('role_name', 50)->nullable();
            $table->boolean('is_active', true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wepos_role');
    }
}
