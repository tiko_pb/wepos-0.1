<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToWeposUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wepos_user', function (Blueprint $table) {
            $table->decimal('created_by', 10, 0)->nullable();
            $table->decimal('updated_by', 10, 0)->nullable();
            $table->string('user_value', 50)->nullable();
            $table->string('user_name', 50)->nullable();
            $table->string('user_password', 50)->nullable();
            $table->boolean('is_active', true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wepos_user', function (Blueprint $table) {
            $table->dropColumn(['created_by', 'updated_by', 'user_value', 'user_name', 'user_password', 'is_active']);
        });
    }
}
