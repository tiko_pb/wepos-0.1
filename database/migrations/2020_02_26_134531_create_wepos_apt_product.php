<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeposAptProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wepos_apt_product', function (Blueprint $table) {
            $table->mediumInteger('wepos_apt_product_id')->autoIncrement();
            $table->decimal('wepos_organization_id', 10, 0);
            $table->timestamp('CREATED_AT',0);
            $table->decimal('created_by', 10, 0);
            $table->timestamp('UPDATED_AT',0);
            $table->decimal('UPDATED_BY', 10, 0);
            $table->boolean('isactive');
            $table->string('value');
            $table->string('name');
            $table->longText('description');
            $table->string('barcode_id');
            $table->decimal('wepos_product_category_id', 10, 0);
            $table->decimal('wepos_uom_id', 10, 0);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wepos_apt_product');
    }
}
