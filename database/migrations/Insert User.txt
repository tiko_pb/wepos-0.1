insert into wepos_user 
(created_by, updated_by, created_at, updated_at, user_value, user_name, user_password, is_active)
values (0, 0, now(),  now(), 'admin', 'Super Admin', 'zxcv', true);

insert into wepos_role
(created_by, updated_by, role_value, role_name, is_active, created_at, updated_at)
values (0 ,0, 'SuperAdmin', 'SuperAdmin', true, now(), now());

insert into wepos_user_role
(wepos_user_id, wepos_role_id, created_by, updated_by, is_active, created_at, updated_at)
values (1, 1, 0, 0, true, now(), now());