<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWepostUom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wepost_uom', function (Blueprint $table) {
            $table->mediumInteger('wepost_uom_id')->autoIncrement();
            $table->decimal('wepos_organization_id', 10, 0);
            $table->timestamp('CREATED_AT',0);
            $table->decimal('created_by', 10, 0);
            $table->timestamp('UPDATED_AT',0);
            $table->decimal('UPDATED_BY', 10, 0);
            $table->boolean('isactive');
            $table->string('value');
            $table->string('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wepost_uom');
    }
}
