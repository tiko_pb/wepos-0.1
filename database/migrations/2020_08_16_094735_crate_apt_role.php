<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrateAptRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apt_role', function (Blueprint $table) {
            $table->mediumInteger('wepos_apt_user_id')->autoIncrement();
            $table->decimal('wepos_organization_id', 10, 0);
            $table->decimal('created_by', 10, 0);
            $table->decimal('updated_by', 10, 0);
            $table->timestamps();
            $table->boolean('is_active', true);
            $table->string('apt_role_value');
            $table->string('apt_role_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apt_role');
    }
}
