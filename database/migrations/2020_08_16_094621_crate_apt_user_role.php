<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrateAptUserRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apt_user_role', function (Blueprint $table) {
            $table->mediumInteger('wepos_apt_user_user_id')->autoIncrement();
            $table->decimal('wepos_organization_id', 10, 0);
            $table->decimal('created_by', 10, 0);
            $table->decimal('updated_by', 10, 0);
            $table->timestamps();
            $table->boolean('is_active', true);
            $table->decimal('apt_user_id', 10, 0);
            $table->decimal('apt_role_id', 10, 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apt_user_role');
    }
}
