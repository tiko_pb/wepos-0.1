<?php

namespace App\Http\Controllers\wepos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Home extends Controller
{
    // public function __construct(Request $request)
    // {    
    //     if($request->session()->has('status_session') !='online'){
    //         return redirect(url('/'));
    //     }
    // }

    public function index(Request $request)
    {
        return view('wepos.menu');
    }
}
