<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\M_dashboard;
use Auth;

class Dashboard extends Controller
{
    function index()
    {
        //return view('layout');
        echo "hai";
    }

    function loginProcess(Request $request)
    {
        $user_value = $request->input('in_user_value');
        $user_password = $request->input('in_user_password');

        $where = array(
            'user_value' => $user_value,
            'user_password' => $user_password
        );
        $M_dashboard = new M_dashboard();
        $user_check = $M_dashboard->loginProcessDB($where)->count();
        $user_profil = $M_dashboard->loginProcessDB($where)->first();
        //echo $user_check;
        if ($user_check > 0){
            $data_session = array (
                'wepos_user_id' => $user_profil->wepos_user_id,
                'user_value' => $user_profil->user_value,
                'status_session' => "online"
            );
            $request->session()->put('userdata',$data_session);
            return redirect(url('/main-home'));
        }else{
            return redirect(url('/'));
        }
        
    }

    function login()
    {
        return view('login');
    }

    function dashboard()
    {
        //$user = DB::select('select * from wepos_user limit 1')->get();
        //echo $user->'wepos_user_id';
    }
}
