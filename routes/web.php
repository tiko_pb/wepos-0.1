<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|   
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Dashboard@login');
Route::post('/login', 'Dashboard@loginProcess');
Route::get('/dashboard', 'Dashboard@dashboard');

//wepos
// Route::group(['middleware' => ['web']], function () {
//     // your routes here
//     Route::get('/main-home', 'wepos\Home@index');
// });

Route::get('/main-home', 'wepos\Home@index');
Route::get('/master-app', 'wepos\App@index');


Route::get('/app', 'wepos\C_app@index');

// Auth::routes();
