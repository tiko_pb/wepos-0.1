<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Login</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <!-- <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/notika/img/favicon.ico') }}"> -->
    <!-- Google Fonts
		============================================ -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('notika/css/bootstrap.min.css') }}">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('notika/css/font-awesome.min.css') }}">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('notika/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('notika/css/owl.theme.css') }}">
    <link rel="stylesheet" href="{{ asset('notika/css/owl.transitions.css') }}">
    <!-- meanmenu CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('notika/css/meanmenu/meanmenu.min.css') }}">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('notika/css/animate.css') }}">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('notika/css/normalize.css') }}">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('notika/css/scrollbar/jquery.mCustomScrollbar.min.css') }}">
    <!-- jvectormap CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('notika/css/jvectormap/jquery-jvectormap-2.0.3.css') }}">
    <!-- notika icon CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('notika/css/notika-custom-icon.css') }}">
    <!-- wave CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('notika/css/wave/waves.min.css') }}">
    <link rel="stylesheet" href="{{ asset('notika/css/wave/button.css') }}">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('notika/css/main.css') }}">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('notika/style.css') }}">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('notika/css/responsive.css') }}">
    <!-- modernizr JS
		============================================ -->
    <script src="{{ asset('notika/js/vendor/modernizr-2.8.3.min.js') }}"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- Login Register area Start-->
    <div class="login-content">
        <!-- Login -->
        <div class="nk-block toggled" id="l-login">
            <form action="{{ url('/login') }}" method="POST">
            @csrf
            <div class="nk-form">
                <div class="input-group">
                    <span class="input-group-addon nk-ic-st-pro"><i class="notika-icon notika-support"></i></span>
                    <div class="nk-int-st">
                        <input type="text" name="in_user_value" class="form-control" placeholder="Username">
                    </div>
                </div>
                <div class="input-group mg-t-15">
                    <span class="input-group-addon nk-ic-st-pro"><i class="notika-icon notika-edit"></i></span>
                    <div class="nk-int-st">
                        <input type="password" name="in_user_password" class="form-control" placeholder="Password">
                    </div>
                </div>
                <div class="input-group mg-t-15" style="align:center;">
                    <button class="btn btn-success notika-btn-success" type="submit">Submit</button>
                </div>
            </div>
            </form>
        </div>
    <!-- Login Register area End-->
    <!-- jquery
		============================================ -->
    <script src="{{ asset('notika/js/vendor/jquery-1.12.4.min.js') }}"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="{{ asset('notika/js/bootstrap.min.js') }}"></script>
    <!-- wow JS
		============================================ -->
    <script src="{{ asset('notika/js/wow.min.js') }}"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="{{ asset('notika/js/jquery-price-slider.js') }}"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="{{ asset('notika/js/owl.carousel.min.js') }}"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="{{ asset('notika/js/jquery.scrollUp.min.js') }}"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="{{ asset('notika/js/meanmenu/jquery.meanmenu.js') }}"></script>
    <!-- counterup JS
		============================================ -->
    <script src="{{ asset('notika/js/counterup/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('notika/js/counterup/waypoints.min.js') }}"></script>
    <script src="{{ asset('notika/js/counterup/counterup-active.js') }}"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="{{ asset('notika/js/scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <!-- sparkline JS
		============================================ -->
    <script src="{{ asset('notika/js/sparkline/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('notika/js/sparkline/sparkline-active.js') }}"></script>
    <!-- flot JS
		============================================ -->
    <script src="{{ asset('notika/js/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('notika/js/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('notika/js/flot/flot-active.js') }}"></script>
    <!-- knob JS
		============================================ -->
    <script src="{{ asset('notika/js/knob/jquery.knob.js') }}"></script>
    <script src="{{ asset('notika/js/knob/jquery.appear.js') }}"></script>
    <script src="{{ asset('notika/js/knob/knob-active.js') }}"></script>
    <!--  Chat JS
		============================================ -->
    <script src="{{ asset('notika/js/chat/jquery.chat.js') }}"></script>
    <!--  wave JS
		============================================ -->
    <script src="{{ asset('notika/js/wave/waves.min.js') }}"></script>
    <script src="{{ asset('notika/js/wave/wave-active.js') }}"></script>
    <!-- icheck JS
		============================================ -->
    <script src="{{ asset('notika/js/icheck/icheck.min.js') }}"></script>
    <script src="{{ asset('notika/js/icheck/icheck-active.js') }}"></script>
    <!--  todo JS
		============================================ -->
    <script src="{{ asset('notika/js/todo/jquery.todo.js') }}"></script>
    <!-- Login JS
		============================================ -->
    <script src="{{ asset('notika/js/login/login-action.js') }}"></script>
    <!-- plugins JS
		============================================ -->
    <script src="{{ asset('notika/js/plugins.js') }}"></script>
    <!-- main JS
		============================================ -->
    <script src="{{ asset('notika/js/main.js') }}"></script>
</body>

</html>