@extends('wepos.layout')

@section('menu')
    <!-- Mobile Menu start -->
    <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                            <ul class="mobile-menu-nav">
                                <li><a data-toggle="collapse" data-target="#demoevent1" href="#">Home</a>
                                  <ul id="demoevent1" class="collapse dropdown-header-top">
                                    <li><a href="#">Dashboard</a></li>
                                  </ul>
                                </li>
                                <li><a data-toggle="collapse" data-target="#demoevent2" href="#">Master Data</a>
                                    <ul id="demoevent2" class="collapse dropdown-header-top">
                                        <li><a href="/app">App</a></li>
                                        <li><a href="#">Partner</a></li>
                                        <li><a href="#">User</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu end -->
    <!-- Main Menu area start-->
    <div class="main-menu-area mg-tb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="nav nav-tabs notika-menu-wrap menu-it-icon-pro">
                        <li class="active"><a data-toggle="tab" href="#Home"><i class="fa fa-home"></i> Home</a>
                        </li>
                        <li><a data-toggle="tab" href="#masterdata"><i class="fa fa-hdd-o"></i> Master Data</a>
                        </li>
                    </ul>
                    <div class="tab-content custom-menu-content">
                        <div id="Home" class="tab-pane in active notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="#">Dashboard</a>
                                </li>
                            </ul>
                        </div>
                        <div id="masterdata" class="tab-pane notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="#">App</a></li>
                                <li><a href="#">Partner</a></li>
                                <li><a href="#">User</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main Menu area End-->
@endsection